<?php

    namespace RAF\RAF\Auth;

    use Exception;
    use RAF\RAF\BaseObject\BaseObject;
    use RAF\RAF\DTO\AuthDTO;
    use RAF\RAF\exceptions\AuthError;
    use RAF\RAF\exceptions\DBError;

    class Auth extends BaseObject
    {

        public function __construct()
        {
            parent::__construct();
            try {
                $this->checkAuthUserTableExists();
                $this->checkAuthTableExists();
            } catch (Exception $e) {
                throw $e;
            }
        }

        /**
         * @param string $authToken
         * @param string $refreshToken
         * @return AuthDTO
         * @throws AuthError
         */
        public function checkTokens($authToken, $refreshToken)
        {
            $db = $this->getDatabase();
            $q = $db->prepare('SELECT * FROM ' . $this->getConfig()['authTokenTable'] . ' WHERE auth_token=? AND refresh_token=?');
            $q->execute([$authToken, $refreshToken]);
            $record = $q->fetch(\PDO::FETCH_ASSOC);
            if (count($record) > 0 && is_array($record)) {
                /** @var AuthDTO $auth */
                $auth = (new \ReflectionClass(new AuthDTO()))->newInstanceArgs($record);
                $now = new \DateTime();
                if ($now->diff($auth->getAuthTokenValidTill())->invert === 0) {
                    return $auth;
                } else {
                    throw new AuthError('Token expired', 403);
                }
            }
            throw new AuthError('Invalid token given.', 400);
        }

        private function checkRefreshToken($token)
        {

        }

        /**
         * @throws DBError
         */
        private function checkAuthTableExists()
        {
            try {
                $db = $this->getDatabase();
                $q = $db->prepare('SELECT 1 FROM ' . $this->getConfig()['authTokenTable']);
                $q->execute();
                $r = $q->fetch(\PDO::FETCH_ASSOC);
                if (!$r) {
                    $this->createAuthTable();
                }
            } catch (DBError $e) {
                throw $e;
            }
        }

        /**
         * @throws DBError
         */
        private function createAuthTable()
        {
            try {
                $db = $this->getDatabase();
                $sql = $this->getParsedSqlTemplate('authTable', [
                    'authTableName' => $this->getConfig()['authTokenTable'],
                    'authUserTableName' => $this->getConfig()['authUserTable']
                ]);
                $q = $db->prepare($sql);
                $q->execute();
            } catch (DBError $e) {
                throw $e;
            }

        }

        /**
         * @throws DBError
         */
        private function checkAuthUserTableExists()
        {
            try {
                $db = $this->getDatabase();
                $q = $db->prepare('SELECT 1 FROM ' . $this->getConfig()['authUserTable']);
                $q->execute();
                $r = $q->fetch(\PDO::FETCH_ASSOC);
                if (!$r) {
                    $this->createAuthUserTable();
                }
            } catch (DBError $e) {
                throw $e;
            }
        }

        /**
         * @throws DBError
         */
        private function createAuthUserTable()
        {
            try {
                $db = $this->getDatabase();
                $sql = $this->getParsedSqlTemplate('userTable', [
                    'authUserTable' => $this->getConfig()['authUserTable'],
                    'authUserTableLoginField' => $this->getConfig()['authUserTableLoginField'],
                    'authUserTablePasswordField' => $this->getConfig()['authUserTablePasswordField']
                ]);
                $q = $db->prepare($sql);
                $q->execute();
            } catch (DBError $e) {
                throw $e;
            }

        }

    }
