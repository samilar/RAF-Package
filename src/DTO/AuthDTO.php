<?php

    namespace RAF\RAF\DTO;

    class AuthDTO extends DTO
    {
        /**
         * @var int
         */
        private $id;
        /**
         * @var int
         */
        private $userId;
        /**
         * @var string
         */
        private $authToken;
        /**
         * @var \DateTime|string
         */
        private $authTokenValidTill;
        /**
         * @var string
         */
        private $refreshToken;
        /**
         * @var \DateTime|string
         */
        private $refreshTokenValidTill;

        public function __construct(
            $id = null,
            $userId = null,
            $authToken = null,
            $authTokenValidTill = null,
            $refreshToken = null,
            $refreshTokenValidTill = null
        ) {
            $this->setId($id);
            $this->setUserId($userId);
            $this->setAuthToken($authToken);
            $this->setAuthTokenValidTill($authTokenValidTill);
            $this->setRefreshToken($refreshToken);
            $this->setRefreshTokenValidTill($refreshTokenValidTill);
        }

        /**
         * @param $id
         * @return $this
         */
        public function setId($id)
        {
            $this->id = $id;
            return $this;
        }
        /**
         * @param $userId
         * @return $this
         */
        public function setUserId($userId)
        {
            $this->userId = $userId;
            return $this;
        }

        /**
         * @param $authToken
         * @return $this
         */
        public function setAuthToken($authToken)
        {
            $this->authToken = $authToken;
            return $this;
        }

        /**
         * @param string $authTokenValidTill
         * @return $this
         */
        public function setAuthTokenValidTill($authTokenValidTill)
        {
            $this->authTokenValidTill = new \DateTime($authTokenValidTill);
            return $this;
        }

        /**
         * @param $refreshToken
         * @return $this
         */
        public function setRefreshToken($refreshToken)
        {
            $this->refreshToken = $refreshToken;
            return $this;
        }

        /**
         * @param string $refreshTokenValidTill
         * @return $this
         */
        public function setRefreshTokenValidTill($refreshTokenValidTill)
        {
            $this->refreshTokenValidTill = new \DateTime($refreshTokenValidTill);
            return $this;
        }

        /**
         * @return int
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @return int
         */
        public function getUserId()
        {
            return $this->userId;
        }

        /**
         * @return string
         */
        public function getAuthToken()
        {
            return $this->authToken;
        }

        /**
         * @param bool $asString
         * @return string|\DateTime
         */
        public function getAuthTokenValidTill($asString = false)
        {
            if ($asString) {
                return $this->authTokenValidTill->format('Y-m-d H:i:s');
            } else {
                return $this->authTokenValidTill;
            }
        }

        /**
         * @return string
         */
        public function getRefreshToken()
        {
            return $this->refreshToken;
        }

        /**
         * @param bool $asString
         * @param string $format
         * @return string|\DateTime
         */
        public function getRefreshTokenValidTill($asString = false, $format = 'Y-m-d H:i:s')
        {
            if ($asString) {
                return $this->refreshTokenValidTill->format($format);
            } else {
                return $this->refreshTokenValidTill;
            }
        }

        /**
         * @return $this
         */
        public function regenerateTokens()
        {
            $this->setAuthToken(substr(md5(microtime() * microtime()), 0, 30));

            $authTokenValidTill = new \DateTime();
            $authTokenValidTill->add(new \DateInterval('PT30M'));
            $this->setAuthTokenValidTill($authTokenValidTill->format('Y-m-d H:i:s'));

            $this->setRefreshToken(substr(md5(microtime() * microtime() * microtime()), 0, 30));

            $refreshTokenValidTill = new \DateTime();
            $refreshTokenValidTill->add(new \DateInterval('PT1H'));
            $this->setRefreshTokenValidTill($refreshTokenValidTill->format('Y-m-d H:i:s'));

            return $this;

        }

        /**
         * @return array
         */
        public function toArrayForInsert()
        {
            return [
                'user_id' => $this->getUserId(),
                'auth_token' => $this->getAuthToken(),
                'auth_token_valid_till' => $this->getAuthTokenValidTill(true),
                'refresh_token' => $this->getRefreshToken(),
                'refresh_token_valid_till' => $this->getRefreshTokenValidTill(true)
            ];
        }

        /**
         * @return array
         */
        public function toArrayForUpdate()
        {
            return ['id' => $this->getId()] + $this->toArrayForInsert();
        }
    }