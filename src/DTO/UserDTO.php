<?php

    namespace RAF\RAF\DTO;

    class UserDTO extends DTO {

        public $id;
        public $login;

        public function __construct(
            $id = null,
            $login = null
        ) {
            $this->setId($id);
            $this->setLogin($login);
        }

        /**
         * @param mixed $id
         * @return $this
         */
        public function setId($id)
        {
            $this->id = $id;
            return $this;
        }

        /**
         * @param $login
         * @return $this
         */
        public function setLogin($login)
        {
            $this->login = $login;
            return $this;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @return mixed
         */
        public function getLogin()
        {
            return $this->login;
        }
    }