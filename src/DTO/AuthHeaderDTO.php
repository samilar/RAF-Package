<?php

    namespace RAF\RAF\DTO;

    class AuthHeaderDTO extends DTO
    {
        private $authToken;
        private $refreshToken;

        public function __construct($authToken = null, $refreshToken = null)
        {
            $this->setAuthToken($authToken);
            $this->setRefreshToken($refreshToken);
        }

        /**
         * @param $authToken
         * @return $this
         */
        public function setAuthToken($authToken)
        {
            $this->authToken = $authToken;
            return $this;
        }

        /**
         * @param $refreshToken
         * @return $this
         */
        public function setRefreshToken($refreshToken)
        {
            $this->refreshToken = $refreshToken;
            return $this;
        }

        /**
         * @return mixed
         */
        public function getAuthToken()
        {
            return $this->authToken;
        }

        /**
         * @return mixed
         */
        public function getRefreshToken()
        {
            return $this->refreshToken;
        }
    }