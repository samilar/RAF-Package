<?php

    namespace RAF\RAF\Request;

    use RAF\RAF\BaseObject\BaseObject;
    use RAF\RAF\interfaces\IRequest;

    abstract class Request extends BaseObject implements IRequest
    {

    }