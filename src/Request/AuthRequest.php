<?php

    namespace RAF\RAF\Request;

    use RAF\RAF\DTO\AuthDTO;
    use RAF\RAF\DTO\AuthHeaderDTO;
    use RAF\RAF\exceptions\DTOError;
    use RAF\RAF\Router\Router;

    class AuthRequest extends Request
    {
        /**
         * @var AuthHeaderDTO
         */
        private $headers;

        public function postAuthLogin($login, $password)
        {
            $db = $this->getDatabase();
            $q = $db->prepare('
SELECT * 
FROM ' . $this->getConfig()['authUserTable'] . ' 
WHERE ' . $this->getConfig()['authUserTableLoginField'] . '=? 
AND ' . $this->getConfig()['authUserTablePasswordField'] . '=?
');
            $q->execute([
                $login,
                call_user_func($this->getConfig()['authPasswordCryptMethod'], $password)
            ]);
            $dbResponse = $q->fetchAll(\PDO::FETCH_ASSOC);
            if (count($dbResponse) > 0) {

                $authTokenDTO = new AuthDTO();
                $authTokenDTO->regenerateTokens();
                $authTokenDTO->setUserId($dbResponse[0]['id']);
                $this->insertInto(
                    $this->getConfig()['authTokenTable'],
                    $authTokenDTO->toArrayForInsert()
                );
                $this->headers = new AuthHeaderDTO(
                    $authTokenDTO->getUserId(),
                    $authTokenDTO->getAuthToken(),
                    $authTokenDTO->getRefreshToken()
                );
                return $dbResponse[0];
            }
            throw new DTOError('Bad credentials.', Router::HTTP_STATUS_NOT_AUTHORIZED);
        }

        /**
         * @return AuthHeaderDTO
         */
        public function getHeaders() {
            return $this->headers;
        }

        public function get() {}

        public function post() {}

        public function put() {}

        public function delete() {}

        public function patch() {}
    }