CREATE TABLE `{{authUserTable}}` (
      `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `{{authUserTableLoginField}}` varchar(200) NOT NULL,
      `{{authUserTablePasswordField}}` varchar(200) NOT NULL
    ) COMMENT='Default RAF Auth User database table' COLLATE 'utf8_general_ci';