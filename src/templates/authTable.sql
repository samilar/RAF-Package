CREATE TABLE `{{authTableName}}` (
      `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `user_id` int(11) NOT NULL,
      `auth_token` text NOT NULL,
      `auth_token_valid_till` datetime NOT NULL,
      `refresh_token` text NOT NULL,
      `refresh_token_valid_till` datetime NOT NULL,
      `created` datetime NOT NULL,
      FOREIGN KEY (`user_id`) REFERENCES `{{authUserTableName}}` (`id`) ON DELETE NO ACTION
    ) COMMENT='Default RAF Auth database table' COLLATE 'utf8_general_ci';