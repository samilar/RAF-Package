<?php

    namespace RAF\RAF\interfaces;

    interface IRequest
    {
        public function get();

        public function post();

        public function put();

        public function delete();

        public function patch();
    }