<?php

    namespace RAF\RAF\Router;

    use RAF\RAF\BaseObject\BaseObject;
    use RAF\RAF\DTO\AuthHeaderDTO;
    use RAF\RAF\exceptions\AuthError;

    class HeadersManager extends BaseObject
    {

        private $authHeaders = [
            'RAF-USER-ID: ',
            'RAF-AUTH-TOKEN: ',
            'RAF-REFRESH-TOKEN: '
        ];
        private $headers = [
            'Access-Control-Allow-Origin: *',
            'Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT',
            'Access-Control-Max-Age: 1000',
            'Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization, accept, client-security-token',
        ];

        /**
         * @param AuthHeaderDTO|null $auth
         */
        public function setHeaders(AuthHeaderDTO $auth = null) {
            if ($auth) {
                foreach ($this->authHeaders as $authHeader) {
                    $parsedHeaderKey = $this->camelize(str_replace('RAF-', '', $authHeader));
                    $methodName = 'get' . rtrim(trim($parsedHeaderKey), ':');
                    if (method_exists($auth, $methodName)) {
                        if ($headerValue = $auth->{$methodName}()) {
                            header($authHeader . $headerValue);
                        }
                    }
                }
            }
            foreach ($this->headers as $header) {
                header($header);
            }
        }

        /**
         * @return AuthHeaderDTO
         * @throws AuthError
         */
        public function readAuthHeaders()
        {
            $authHeaderDTO = new AuthHeaderDTO();
            $requestHeaders = apache_request_headers();
            foreach ($this->authHeaders as $authHeader) {
                if (isset($requestHeaders[rtrim(trim($authHeader), ':')])) {
                    $parsedHeaderKey = $this->camelize(str_replace('RAF-', '', $authHeader));
                    $headerValue = $requestHeaders[rtrim(trim($authHeader), ':')];
                    $methodName = 'set' . rtrim(trim($parsedHeaderKey), ':');
                    if(method_exists($authHeaderDTO, $methodName)) {
                        $authHeaderDTO->{$methodName}($headerValue);
                    }
                }
            }
            return $authHeaderDTO;
        }

    }