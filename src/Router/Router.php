<?php

    namespace RAF\RAF\Router;

    use Exception;
    use RAF\RAF\Auth\Auth;
    use RAF\RAF\BaseObject\BaseObject;
    use RAF\RAF\DTO\UserDTO;
    use RAF\RAF\exceptions\AuthError;
    use RAF\RAF\exceptions\RequestError;
    use RAF\RAF\interfaces\IDTO;
    use RAF\RAF\Request\AuthRequest;
    use ReflectionMethod;

    class Router extends BaseObject
    {

        const HTTP_STATUS_OK = 200;
        const HTTP_STATUS_BAD_REQUEST = 400;
        const HTTP_STATUS_NOT_AUTHORIZED = 401;
        const HTTP_STATUS_NOT_FOUND = 404;
        const HTTP_STATUS_METHOD_NOT_ALLOWED = 405;
        const HTTP_STATUS_INTERNAL_SERVER_ERROR = 500;
        const HTTP_STATUS_NOT_IMPLEMENTED = 501;

        const ALLOWED_REQUEST_WITH_FIELDS = [
            'POST',
            'PUT',
            'PATCH'
        ];

        /**
         * @var HeadersManager
         */
        protected $headersManager;

        private $requestMethod;
        private $uri;
        private $parsedUri;
        private $className;
        private $classMethod;
        private $classMethodParameters;
        private $classMethodPostPutPatchParameters;

        private $server;
        private $request;
        /**
         * @var Auth
         */
        private $auth;

        public function __construct() {
            parent::__construct();
            try {
                $this->init($_SERVER, $_REQUEST);
            } catch (Exception $e) {
                http_response_code($e->getCode());
                echo json_encode([
                    'message' => $e->getMessage(),
                    'code'    => $e->getCode()
                ]);
            }
        }

        private function init($server, $request) {
            $this->headersManager = new HeadersManager();
            $this->server = $server;
            $this->request = $request;
            $this->parseServer();
            $this->auth = new Auth();
        }

        private function parseServer() {
            $this->parsedUri = explode('/', rtrim(preg_replace('~/{2,}~', '/',
                substr(explode('?', $this->uri = $this->server['REQUEST_URI'])[0], 1)), '/'));

            $this->requestMethod = $this->server['REQUEST_METHOD'];
            $this->className = $this->camelize($this->parsedUri[0]) . 'Request';
            $this->classMethod = strtolower($this->server['REQUEST_METHOD']) . $this->camelize($this->parsedUri[0]) . (count($this->parsedUri) > 1 && $this->parsedUri[1] ? $this->camelize($this->parsedUri[1]) : '');
            $this->classMethodParameters = [];
            $this->classMethodPostPutPatchParameters = [];

            if (count($this->parsedUri) > 2) {
                $this->classMethodParameters = array_slice($this->parsedUri, 2);
            }
            if (in_array($this->requestMethod, self::ALLOWED_REQUEST_WITH_FIELDS)) {
                $this->classMethodPostPutPatchParameters = $this->request;
            }
        }

        public function run() {
            try {
                if (isset($this->getConfig()['auth']) && $this->getConfig()['auth'] === '1' && !in_array($this->uri, $this->getConfig()['allowedAccess'])) {
                    $headers = $this->headersManager->readAuthHeaders();
                    if ($headers->getAuthToken() && $headers->getRefreshToken()) {
                        $this->auth->checkTokens($headers->getAuthToken(), $headers->getRefreshToken());
                    } else {
                        throw new AuthError('Unauthorized', self::HTTP_STATUS_NOT_AUTHORIZED);
                    }
                    $this->headersManager->setHeaders($headers);
                }

                if ($this->classMethod && $this->className) {
                    $namespacedClassName = '\\Request\\' . $this->className;
                    $class = new $namespacedClassName();
                    if (method_exists($class, $this->classMethod)) {
                        $reflection = new ReflectionMethod($class, $this->classMethod);
                        $parameters = $this->classMethodPostPutPatchParameters ? $this->classMethodPostPutPatchParameters : $this->classMethodParameters;
                        $arguments = [];
                        foreach ($reflection->getParameters() as $key => $arg) {
                            if (isset($parameters[$arg->name]) || isset($parameters[$key])) {
                                $arguments[$arg->name] = isset($parameters[$arg->name]) ? $parameters[$arg->name] : $parameters[$key];
                            } else {
                                if ($arg->isDefaultValueAvailable()) {
                                    $arguments[$arg->name] = $arg->getDefaultValue();
                                } else {
                                    $arguments[$arg->name] = null;
                                }
                            }
                        }
                        if (count($reflection->getParameters()) <= count($parameters)) {
                            $response = call_user_func_array([$class, $this->classMethod], $arguments);
                            if ($response instanceof UserDTO && $class instanceof AuthRequest) {
                                $this->headersManager->setHeaders($class->getHeaders());
                            }
                            if ($response instanceof IDTO) {
                                // Show output
                                http_response_code(self::HTTP_STATUS_OK);
                                echo json_encode($response);
                            }
                        } else {
                            $paramStr = '';
                            foreach ($reflection->getParameters() as $key => $parameter) {
                                $part2 = $glue = '';
                                $boolean = ['true', 'false'];
                                if (array_key_exists($parameter->name, $parameters)) {
                                    if (!is_numeric($parameters[$parameter->name]) && is_string($parameters[$parameter->name]) && !in_array($parameters[$parameter->name],
                                            $boolean)
                                    ) {
                                        $glue = '"';
                                    }
                                    $part2 = ' = ' . $glue . $parameters[$parameter->name] . $glue;
                                } elseif (array_key_exists($key, $parameters)) {
                                    if (!is_numeric($parameters[$key]) && is_string($parameters[$key]) && !in_array($parameters[$key],
                                            $boolean)
                                    ) {
                                        $glue = '"';
                                    }
                                    $part2 = ' = ' . $glue . $parameters[$key] . $glue;
                                }
                                $paramStr .= '$' . $parameter->name . $part2 . ',';
                            }
                            throw new RequestError('Parameters mismatch. Method ' . $this->classMethod . '(' . rtrim($paramStr,
                                    ',') . ') receive this INPUT = ' . serialize($parameters),
                                self::HTTP_STATUS_BAD_REQUEST);
                        }
                    } else {
                        throw new RequestError('Not implemented. [class:: ' . $this->className . ', method:: ' . $this->classMethod . ']',
                            self::HTTP_STATUS_NOT_IMPLEMENTED);
                    }
                }
            } catch (Exception $e) {
                http_response_code($e->getCode());
                echo json_encode([
                    'message' => $e->getMessage(),
                    'code'    => $e->getCode()
                ]);
            }
        }

    }
