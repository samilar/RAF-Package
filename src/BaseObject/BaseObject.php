<?php

    namespace RAF\RAF\BaseObject;

    use RAF\RAF\exceptions\DBError;

    class BaseObject
    {
        protected $vendorDir;
        protected $appDir;
        protected $rafDir;

        private $db;
        private $config;

        public function __construct() {
            $this->initApp();
        }

        private function initApp() {
            if (!$this->rafDir) {
                $this->rafDir = dirname(__DIR__);
            }
            if (!$this->vendorDir) {
                $this->vendorDir = dirname(dirname(__DIR__));
            }
            if (!$this->appDir) {
                $this->appDir = dirname(dirname(dirname($this->vendorDir)));
            }
            if (!$this->db) {
                $this->db = $this->getDatabase();
            }
            if (!$this->config) {
                $this->config = $this->getConfig();
            }
        }

        /**
         * @return \PDO
         * @throws DBError
         */
        public function getDatabase() {
            if (!$this->db) {
                try {
                    $this->db = new \PDO(
                        'mysql:host=' . $this->getConfig()['host'] . ';dbname=' . $this->getConfig()['db'],
                        $this->getConfig()['user'],
                        $this->getConfig()['password']
                    );
                    $this->db->exec("set names " . $this->getConfig()['charset']);
                } catch (DBError $e) {
                    throw $e;
                }
            }
            return $this->db;
        }

        protected function camelize($string)
        {
            $chunks = explode('-', $string);
            $response = '';
            foreach($chunks as $chunk) {
                $response .= ucfirst(strtolower($chunk));
            }
            return $response;
        }

        /**
         * @return array
         * @throws DBError
         */
        protected function getConfig()
        {
            if (!$this->config) {
                if (file_exists($this->appDir . '/config/config.ini') && !file_exists($this->appDir . '/config/config.local.ini')) {
                    $this->config = parse_ini_file($this->appDir . '/config/config.ini');
                } elseif (file_exists($this->appDir . '/config/config.local.ini')) {
                    $this->config = parse_ini_file($this->appDir . '/config/config.local.ini');
                } else {
                    throw new DBError('Unable to get config file ( ' . $this->appDir . '/config/config.ini or ' . $this->appDir . '/config/config.local.ini )!',
                        500);
                }
            }
            return $this->config;
        }

        /**
         * @param string $tableName
         * @param array $parameters
         * @return bool
         */
        public function insertInto($tableName, $parameters)
        {
            $fields = array_keys($parameters); // here you have to trust your field names!
            $values = array_values($parameters);
            $fieldList = implode(',',$fields);
            $qs = rtrim(str_repeat("?,",count($fields)), ',');
            $sql = 'INSERT INTO ' . $tableName . ' (' . $fieldList . ') VALUES(' . $qs . ')';
            $i = $this->getDatabase()->prepare($sql);
            return $i->execute($values);
        }

        protected function getParsedSqlTemplate($templateFile, $args)
        {
            $filePath = $this->rafDir . '/templates/' . $templateFile . '.sql';
            if (file_exists($filePath)) {
                $fileContent = trim(file_get_contents($filePath));
                $fileContent = trim(preg_replace('/\s\s+/', ' ', $fileContent));
                $fileContent = trim(preg_replace('/\s+/', ' ', $fileContent));
                foreach ($args as $key => $value) {
                    $fileContent = preg_replace('/\{\{' . $key . '\}\}/', $value, $fileContent);
                }
                return $fileContent;
            }
            return false;
        }

    }